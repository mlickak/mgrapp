import json
from dataclasses import dataclass


class Singleton(type):  # meta-klasa
    _instances = {}  # słownik instacji klas tworzonych

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


cfgPL = r'''
    {
    "languageCode" : "PL",
    "signin" : "Zaloguj",
    "signout" : "Wyloguj",
    "username" : "Nazwa użytkownika",
    "password" : "Hasło",
    "quit" : "Wyjdź",
    "mainmenu" : "Menu główne",
    "createexam" : "Stwórz egzamin",
    "ret" : "Wróć",
    "open" : "Otwarte",
    "multiple" : "Wielokrotnego wyboru"
    }
'''

cfgEN = r'''
    {
    "languageCode" : "EN",
    "signin" : "Sign in",
    "signout" : "Sign out"
    "username" : "Username",
    "password" : "Password",
    "quit" : "Quit",
    "mainmenu" : "Main menu",
    "createexam" : "Create exam",
    "ret" : "Return",
    "open" : "Open",
    "multiple" : "Multiple"
    }
'''


@dataclass
class Language(metaclass=Singleton):
    languageCode: str = "EN"
    signin: str = "Sign in"
    signout: str = "Sign out"
    username: str = "Username"
    password: str = "Password"
    quit: str = "Quit"
    mainmenu: str = "Main menu"
    createexam: str = "Create exam"
    ret: str = "Return"
    open: str = "Open"
    multiple: str = "Multiple"

    @classmethod
    def set_language(cls, lang: str):
        cls.__init__(cls, **json.loads(globals().get(f'cfg{lang.upper()}')))  # wyciąga z jsona jako str zmienną globalną typu cfg


"""print(Language.login)
Language.set_language('PL')
print(Language.login)
Language.set_language('EN')
print(Language.login)
"""
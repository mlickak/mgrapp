import datetime
from dataclasses import dataclass
from threading import Lock
import time
import json
from random import randrange


@dataclass
class Exam:
    static_id = 999990

    _exam_id: int  # every
    professor_id: int  # only
    title: str
    # creation_date: datetime
    # start_date: datetime
    # deadline: datetime
    # max_points: int
    questions: dir

    # addressee

    def __init__(self, title, questions):
        self._exam_id = randrange(1100000) - 100000
        self.title = title
        # self.creation_date = datetime.datetime.now()
        # self.start_date = start_date
        # self.end_date = end_date
        self.questions = questions
        # print('EXAM : \"', title, ' number of questions : ', len(questions))

    def get_exam_id(self) -> int:
        return self._exam_id

    def get_json(self) -> str:
        # print('QQQ\n', self.questions)
        dir_obj = {'title': self.title,
                   'id': self._exam_id,
                   'questions': self.questions}
        # print(dir_obj)
        json_obj = json.dumps(dir_obj, indent=4)
        return json_obj

import requests
import json


class User:
    id: int
    is_professor: bool
    first_name: str
    last_name: str

    static_id = 888880

    def __init__(self, is_professor: bool, password: str, first_name: str, last_name: str):
        # from User import User
        # import requests
        # User(False, 'masło', 'Kasia', 'Studentka')

        # student -> masło
        # profesor -> woda123

        self.id = 222111  #self.set_id(self.static_id)
        self.is_professor = is_professor

        url = 'http://127.0.0.1:5000/sign-up'
        obj = {
            'id': str(self.id),
            'ut': str(self.is_professor),
            'password': str(password),
            'first_name': first_name,
            'last_name': last_name
        }
        json_obj = json.dumps(obj, indent=4)

        try:
            res = requests.post(url, json=json_obj)
            print(res.text)
        except:
            print('Request send failed. Try again.')

    @staticmethod
    def set_id(static_id) -> int:
        try:
            return static_id
        finally:
            static_id += 1

    def get_user_type(self) -> bool:
        return self.is_professor

    def get_id(self):
        return self.id

    def print_user(self):
        print(self.first_name + self.last_name + '[' + str(self.is_professor) + ']' + self.id)

    def get_self(self):
        return self

import tkinter as tk
from tkinter import ttk
from Question import Question, MultipleQuestion, OpenQuestion, SingleQuestion, PyramidQuestion
from typing import Union
import threading
import time


class AnswerFrame(tk.LabelFrame):
    current_question_content: tk.Label
    open_answer_text: tk.Text
    multiple_answer_variants: list
    timeout: float
    answers_handle: list
    question_number: str
    current_question: Union[None, dir]
    toggles: list
    radiobutton_toggle: tk.IntVar
    multiple_pyramid_toggles: list
    previous_radiobutton_choice: Union[None, int]
    timer: threading

    pen_color = '#6162FF'
    frame_width = 520
    distance_between_rows = 0.17
    toggle_place = 0.12
    answers_place = toggle_place + 0.08

    def __init__(self, window, gui_controller, **kw):
        super().__init__(window, font=gui_controller.q_font, **kw)
        self.config(width=self.frame_width, height=340)
        self.controller = gui_controller
        self.go_forward_handle = window.go_forward

        self.current_question_content = tk.Label(self, font=self.controller.super_q_font,
                                                 height=2, wraplength=self.frame_width - 20)
        self.current_question_content.place(relx=0.5, rely=0.15, anchor=tk.CENTER)
        self.current_question = None
        self.previous_radiobutton_choice = None
        self.radiobutton_toggle = tk.IntVar()
        self.multiple_answer_variants = []
        self.toggles = []
        self.multiple_pyramid_toggles = []
        self.canvas = tk.Canvas(self, width=self.frame_width, height=5, bg='red')
        self.canvas.place(relx=0.5, rely=0.03, anchor=tk.CENTER)

        self.open_answer_text = tk.Text(self, width=60,
                                        height=10, wrap=tk.WORD)
        self.open_answer_text.bind('<Control-x>', lambda e: 'break')  # disable cut
        self.open_answer_text.bind('<Control-c>', lambda e: 'break')  # disable copy
        self.open_answer_text.bind('<Control-v>', lambda e: 'break')  # disable paste
        self.open_answer_text.bind('<Button-3>', lambda e: 'break')  # disable right-click

    def set_answer_handle_list(self, answer_list) -> None:
        self.answers_handle = answer_list

    def change_question(self, question, number, maximum):
        """ change current question in answer frame for new one """

        if number != 0:
            self.safe_result()

        # cleaning after previous question
        self.clean_multiple_variants()
        self.multiple_answer_variants = []
        self.toggles = []
        self.multiple_pyramid_toggles = []
        self.question_number = number
        self.config(text='Pytanie ' + str(number + 1) + ' / ' + str(maximum))

        # configure for new question
        self.current_question = question
        self.current_question_content.configure(text=self.current_question['content'])
        self.timeout = self.current_question['time']
        if self.current_question['type'] != 'SingleQuestion':
            self.open_answer_text.place_forget()

        # question types
        if self.current_question['type'] == 'OpenQuestion':
            self.open_answer_text.insert(1.0, '')
            self.open_answer_text.place(relx=0.5, rely=0.55, anchor=tk.CENTER)

        elif self.current_question['type'] == 'SingleQuestion':  # radiobutton
            self.set_answers_variants(question['available_answers'])
            self.radiobutton_toggle.set(None)
            for i in range(len(self.multiple_answer_variants)):
                self.toggles.append(tk.Radiobutton(self, variable=self.radiobutton_toggle, value=i,
                                                   command=self.click_on_click_off))
            self.place_toggles()

        elif self.current_question['type'] == 'MultipleQuestion':
            self.set_answers_variants(question['available_answers'])
            for i in range(len(self.multiple_answer_variants)):
                toggle_var = tk.IntVar()
                self.toggles.append(tk.Checkbutton(self, variable=toggle_var))
                self.multiple_pyramid_toggles.append(toggle_var)
            self.place_toggles()

        elif self.current_question['type'] == 'PyramidQuestion':
            # self.set_multiple_variants(question['available_answers'])
            self.set_answers_variants(question['answers_content'])  # in next exams changed to previous line
            for i in range(len(self.multiple_answer_variants)):
                toggle_var = tk.IntVar()
                self.toggles.append(ttk.Combobox(self, state='readonly', values=PyramidQuestion.get_pyramid_values(4),
                                                 width=3, font=self.controller.big_font))
                self.multiple_pyramid_toggles.append(toggle_var)
            self.place_toggles()
        else:
            print('fail type')
            exit()
        self.start_timer_thread()

    def click_on_click_off(self) -> None:
        if self.previous_radiobutton_choice == self.radiobutton_toggle.get():
            self.previous_radiobutton_choice = self.radiobutton_toggle.get()
            self.radiobutton_toggle.set(None)
        else:
            self.previous_radiobutton_choice = self.radiobutton_toggle.get()

    def set_answers_variants(self, var_list) -> None:
        for variant in var_list:
            self.multiple_answer_variants.append(tk.Label(self,
                                                          text=variant,
                                                          # font=self.controller.bigger_font,
                                                          height=2))
        for i in range(len(self.multiple_answer_variants)):
            self.multiple_answer_variants[i].place(relx=self.answers_place,
                                                   rely=0.3 + i * self.distance_between_rows,
                                                   anchor='w')

    def place_toggles(self):
        for i in range(len(self.toggles)):
            self.toggles[i].place(relx=self.toggle_place, rely=0.3 + i * self.distance_between_rows, anchor=tk.CENTER)

    def clean_multiple_variants(self) -> None:
        if len(self.multiple_answer_variants) != 0:
            for label in self.multiple_answer_variants:
                label.place_forget()
        if len(self.toggles) != 0:
            for toggle in self.toggles:
                toggle.place_forget()

    def get_variants_from_toggles(self) -> list:
        int_var_list = []
        for t in self.multiple_pyramid_toggles:
            int_var_list.append(t.get())
        return int_var_list

    def safe_result(self) -> None:
        if self.current_question['type'] == 'OpenQuestion':
            self.current_question['answer'] = self.open_answer_text.get(1.0, tk.END)
        elif self.current_question['type'] == 'SingleQuestion':
            try:
                self.current_question['answer'] = self.radiobutton_toggle.get()
            except:
                self.current_question['answer'] = None
        elif self.current_question['type'] == 'MultipleQuestion':
            self.current_question['answer'] = self.get_variants_from_toggles()
        elif self.current_question['type'] == 'PyramidQuestion':
            self.current_question['answer'] = self.get_variants_from_toggles()
        else:
            pass
        # print('numba', int(self.question_number))
        # print('al', len(self.answers_handle))
        self.answers_handle[int(self.question_number)] = {'place': self.current_question['place'],
                                                          'type': self.current_question['type'],
                                                          'answer': self.current_question['answer']}

    def start_timer_thread(self) -> None:
        self.timer = threading.Thread(target=self.question_countdown, args=(self.question_number,))
        self.timer.start()

    def question_countdown(self, identity) -> None:
        refreshing = 1
        try:
            while self.timeout:  # while t > 0 for clarity
                if identity != self.question_number:
                    break
                time_bar = int(self.frame_width * ((self.timeout - 1) / self.current_question['time']))
                self.canvas.config(width=time_bar)
                # print('Thread', identity, 'on the run', secs, end="\r")  # overwrite previous line
                time.sleep(refreshing / 60)
                self.timeout -= refreshing / 60  # 1
                if self.timeout < 1:  # without this timeout will be drop to infinity
                    self.timeout = 0.0
            if self.timeout == 0.0:
                self.go_forward_handle()
        except RuntimeError:  # in case user force to close the program
            pass


from enum import Enum


class UserType(Enum):
    Student = 1
    Professor = 2

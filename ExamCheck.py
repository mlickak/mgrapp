import tkinter as tk
from tkinter import ttk, messagebox
from AnswerToCheckFrame import AnswerToCheckFrame as ATCF


class ExamCheck(tk.Frame):
    controller: tk.Tk
    blue_color = '#6162FF'
    background = '#ececec'

    quest_in_frames: list

    def __init__(self, parent, controller, bg):
        super().__init__(parent, bg=bg, width=controller.get_frame_width(), height=controller.get_frame_height())
        self.controller = controller

        self.quest_in_frames = list()

        """ Title """
        frame_title = tk.Label(self, text='Sprawdź egzamin studenta\n', font=self.controller.title_font, bg='white')
        frame_title.place(relx=0.5, rely=0.1, anchor=tk.CENTER)

        """s"""
        self.index_label = tk.Label(self, text='Wprowadź indeks egzaminu', bg=bg)
        self.index_label.place(relx=0.25, rely=0.25, anchor=tk.CENTER)
        self.exam_index = tk.IntVar()
        self.exam_index_entry = tk.Entry(self, textvariable=self.exam_index, bg=self.background, width=6)
        self.exam_index_entry.place(relx=0.25, rely=0.3, anchor=tk.CENTER)

        self.exam_label = tk.Label(self, text='Wprowadź indeks studenta', bg=bg)
        self.exam_label.place(relx=0.5, rely=0.25, anchor=tk.CENTER)
        self.student_index = tk.IntVar()
        self.student_index_entry = tk.Entry(self, textvariable=self.student_index, bg=self.background, width=6)
        self.student_index_entry.place(relx=0.5, rely=0.3, anchor=tk.CENTER)

        self.get_button = tk.Button(self, text='Pobierz egzamin', command=self.get_exam, width=17, bg=self.blue_color,
                                    fg=bg, bd=0)
        self.get_button.place(relx=0.75, rely=0.275, anchor=tk.CENTER)

        """ Scrollbar """
        wrapper1 = tk.LabelFrame(self)
        wrapper2 = tk.LabelFrame(self)

        self.canvas = tk.Canvas(wrapper1, width=530, height=250)
        self.canvas.pack(side=tk.LEFT)
        self.y_scrollbar = ttk.Scrollbar(wrapper1, orient='vertical', command=self.canvas.yview)
        self.y_scrollbar.pack(side=tk.RIGHT, fill='y')
        self.canvas.configure(yscrollcommand=self.y_scrollbar.set)
        self.canvas.bind('<Configure>', lambda e: self.canvas.configure(scrollregion=self.canvas.bbox('all')))
        self.scrollbar_frame = tk.Frame(self.canvas)
        self.canvas.create_window((0, 0), window=self.scrollbar_frame, anchor='nw')

        wrapper1.place(relx=0.5, rely=0.55, anchor=tk.CENTER)  # pack(fill='both', expand='yes', padx=10, pady=10)
        wrapper2.place(relx=0.5, rely=0.55, anchor=tk.CENTER)  # pack(fill='both', expand='yes', padx=10, pady=10)

        """Approval"""
        self.approval_button = tk.Button(self, text='Gotowe', command=self.approval,font=self.controller.button_font,
                                     width=10,
                                     bg=self.blue_color,
                                     fg=bg,
                                     bd=0)
        self.approval_button.place(relx=0.85, rely=0.84, anchor=tk.CENTER)

        """Quit app"""
        self.quit_button = tk.Button(self,
                                     text='Wyjdź',
                                     command=self.quit_exam_check,
                                     font=self.controller.button_font,
                                     width=10,
                                     bg=self.blue_color,
                                     fg=bg,
                                     bd=0)
        self.quit_button.place(relx=0.85, rely=0.9, anchor=tk.CENTER)

    def get_exam(self):
        # clean last exam
        # get from database
        self.add_new_question()

    def quit_exam_check(self):
        self.controller.show_frame('MainMenu')

        # cleaning

    def add_new_question(self) -> None:
        #scrollbar_holder, controller_holder, number, content, is_correct, is_open, is_pyramid, **kw):
        frame = ATCF(scrollbar_holder=self.scrollbar_frame,
                     controller_holder=self.controller,
                     number=1,
                     q_content='Pytanie otwarte?',
                     a_content='Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam',
                     is_correct=None,
                     is_open=True,
                     is_multiple=False,
                     is_pyramid=False)
        frame.pack(fill=tk.BOTH, anchor=tk.CENTER)
        self.quest_in_frames.append(frame)

        frame = ATCF(scrollbar_holder=self.scrollbar_frame,
                     controller_holder=self.controller,
                     number=2,
                     q_content='Pytanie oceniane automatycznie',
                     a_content=['Fajna odpowiedź', 'Lepsza odpowiedź', 'Nie wiem'],
                     is_correct=[True, False, False],
                     is_open=False,
                     is_multiple=True,
                     is_pyramid=True)
        frame.pack(fill=tk.BOTH, anchor=tk.CENTER)
        self.quest_in_frames.append(frame)

        self.customize_scrollbar()

    def customize_scrollbar(self) -> None:
        self.canvas.configure(yscrollcommand=self.y_scrollbar.set, scrollregion=self.canvas.bbox("all"))
        self.canvas.create_window((0, 0), window=self.scrollbar_frame, anchor='nw')

    def approval(self):
        for q in self.quest_in_frames:
            q.checkpoint()

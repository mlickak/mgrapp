"""Contains main class"""

import tkinter as tk
from tkinter import font as tk_font
from SignIn import SignIn
from MainMenu import MainMenu
from User import User
from ExamCreator import ExamCreator
from ApproachToExamFrame import ApproachToExam
from GradeCheck import GradeCheck
from ExamCheck import ExamCheck
import ctypes
import requests
import json
from requests.exceptions import HTTPError


class GuiController(tk.Tk):
    container: tk.Frame
    window_height: int
    window_width: int
    url: str
    frame_width: int
    frame_height: int

    user_token: str
    user_token = 'tokenoeno'#None
    user_type = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)  # other way --> tk.Tk.__init__(self, *args, **kwargs)

        self.bind('<Win_L>', self.escape_through_window_management)
        self.bind('<Win_R>', self.escape_through_window_management)
        # self.bind('<Alt_L-Tab>', self.escape_through_window_management)
        # self.bind('<Alt_R-Tab>', self.escape_through_window_management)
        # self.bind('<KeyRelease>', self.escape_through_window_management)

        self.bind('<Escape>', lambda e: 'break')

        self.url = 'http://127.0.0.1:5000'

        self.title_font = tk_font.Font(family='Helvetica', size=20, weight="bold", slant="italic")
        self.title_font_2 = tk_font.Font(family='Helvetica', size=16)
        self.font_to_write_with = tk_font.Font(family='Helvetica', size=11, slant="italic")
        self.super_q_font = tk_font.Font(family='Helvetica', size=11, weight='bold')
        self.q_font = tk_font.Font(family='Helvetica', size=11)
        self.button_font = tk_font.Font(family='Helvetica', size=11, weight='bold')
        self.big_font = tk_font.Font(family='Helvetica', size=10)
        self.bigger_font = tk_font.Font(family='Helvetica', size=12)

        self.bg_color = 'white'

        """App window attributes"""
        self.overrideredirect(True)  # removes title bar
        # self.wm_attributes('-fullscreen', 'true')
        self.resizable(0, 0)
        self.attributes("-topmost", True)

        """Size """
        user32 = ctypes.windll.user32
        x, y = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
        self.frame_height = 600
        self.frame_width = 800
        self.window_height = 600
        self.window_width = 800
        distance_left = int((x - self.window_width)/2)
        distance_up = int((y - self.window_height)/2)-20
        self.geometry(str(self.window_width) + 'x' + str(self.window_height) +
                      '+' + str(distance_left) + '+' + str(distance_up))
        # self.attributes('-fullscreen', True) # fullscreen

        """the container is where we'll stack a bunch of frames
        on top of each other, then the one we want visible
        will be raised above the others"""
        self.container = tk.Frame()
        self.container.pack(side="top", expand=True)  # fill="both",
        self.container.grid_rowconfigure(0, weight=1)
        self.container.grid_columnconfigure(0, weight=1)
        self.all_frames = {}

        for ThisFrame in (SignIn, MainMenu, ExamCreator, ApproachToExam, GradeCheck, ExamCheck):
            page_name = ThisFrame.__name__
            frame = ThisFrame(parent=self.container, controller=self, bg=self.bg_color)  # constructors
            self.all_frames[page_name] = frame
            """put all of the pages in the same location;
            the one on the top of the stacking order
            will be the one that is visible."""
            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame('SignIn')

    def show_frame(self, page_name) -> None:
        frame = self.all_frames[page_name]
        frame.tkraise()

    def set_token(self, token) -> None:
        self.user_token = token

    def get_token(self) -> str:
        return self.user_token

    def get_url(self) -> str:
        return self.url

    def get_frame_width(self) -> str:
        return str(self.frame_width)

    def get_frame_height(self) -> str:
        return str(self.frame_height)

    def reset_user_type(self, index):
        user_type = None
        try:
            # print('tried')
            obj = {'id': index, 'token': self.get_token()}
            json_obj = json.dumps(obj, indent=4)
            url = self.get_url() + '/auth/get-type'
            # print(url)
            r = requests.get(url, json=json_obj)
            user_type = r.text
            self.user_type = user_type
            self.all_frames['MainMenu'].replace_widgets(self.user_type)
            return

        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')  # Python 3.6
        except Exception as err:
            print(f'Other error occurred: {err}\nDatabase is propably off')  # Python 3.6

        if user_type == '' or user_type is None:
            self.user_type = None
            self.all_frames['MainMenu'].replace_widgets(self.user_type)
            return
        else:
            return

    def escape_through_window_management(self, event):
        print(event)
        self.all_frames['ApproachToExam'].window_escape()

    def emergency_logout(self):
        self.show_frame('SignIn')
        self.set_token(None)

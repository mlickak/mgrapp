import tkinter as tk
import json
import requests


class GradeCheck(tk.Frame):
    controller: tk.Tk
    blue_color = '#6162FF'
    background = '#ececec'

    def __init__(self, parent, controller, bg):
        super().__init__(parent, bg=bg, width=controller.get_frame_width(), height=controller.get_frame_height())
        self.controller = controller

        """ Title """
        frame_title = tk.Label(self,
                               text='Sprawdź wynik egzaminu\n',
                               font=self.controller.title_font,
                               bg=bg)
        frame_title.place(relx=0.5, rely=0.1, anchor=tk.CENTER)

        self.exam_label = tk.Label(self, text='Wprowadź indeks egzaminu', bg=bg)
        self.exam_label.place(relx=0.33, rely=0.25, anchor=tk.CENTER)
        self.exam_index = tk.IntVar()
        self.exam_index_entry = tk.Entry(self, textvariable=self.exam_index, bg=self.background, width=6)
        self.exam_index_entry.place(relx=0.33, rely=0.3, anchor=tk.CENTER)

        self.get_button = tk.Button(self, text='Pobierz egzamin', command=self.get_grade, width=17, bg=self.blue_color,
                                    fg=bg, bd=0)
        self.get_button.place(relx=0.66, rely=0.275, anchor=tk.CENTER)

        """ results """
        scroll = tk. Scrollbar(self)
        self.results = tk.Text(self, width=60, height=15)
        scroll.place(relx=0.85, rely=0.6, anchor=tk.CENTER)
        self.results.place(relx=0.5, rely=0.6, anchor=tk.CENTER)
        scroll.config(command=self.results.yview)
        self.results.config(yscrollcommand=scroll.set)

        # self.results.config(state='disabled')

        # quote = """HAMLET: To be, or not to be--that is the question:
        # Whether 'tis nobler in the mind to suffer
        # The slings and arrows of outrageous fortune
        # Or to take arms against a sea of troubles
        # And by opposing end them. To die, to sleep--
        # No more--and by a sleep to say we end
        # The heartache, and the thousand natural shocks
        # That flesh is heir to. 'Tis a consummation
        # Devoutly to be wished."""

        """ Quit button """
        self.quit_button = tk.Button(self,
                                     text='Wyjdź',
                                     command=self.quit_exam_check,
                                     font=self.controller.button_font,
                                     width=10,
                                     bg=self.blue_color,
                                     fg=bg,
                                     bd=0)
        self.quit_button.place(relx=0.9, rely=0.92, anchor=tk.CENTER)

    def get_grade(self):
        """Request for grade"""
        self.results.delete(1.0, tk.END)

        url = self.controller.get_url() + '/manage/get-grade'
        obj = {
            'exam_id': self.exam_index.get(),
            'token': self.controller.get_token()
        }
        json_obj = json.dumps(obj, indent=4)

        try:
            r = requests.get(url, json=json_obj)
            data = r.json()

            string_text = data['title'] + '\n'
            if data['grade'] > 3:
                string_text += 'Ocena : ' + str(data['grade']) + ' (zaliczone)'
            else:
                string_text += 'Ocena : ' + str(data['grade']) + ' (niezaliczone)'

            string_text += '\nPunktacja : ' + str(data['points']) + '/' + str(data['max_p'])
            # for a in data['ans']:
            #     print(string_text)
            #     string_text += a

            print(data)
            self.results.insert(tk.END, string_text)

        except Exception as err:
            print(f'Other error occurred: {err}\nDatabase is propably off')  # Python 3.6
        except:
            print('Request send failed. Try again.')

    def quit_exam_check(self) -> None:
        self.controller.show_frame('MainMenu')
        # cleaning
        self.exam_index.set('')

import tkinter as tk
from tkinter import ttk
from Question import Question, MultipleQuestion, OpenQuestion, SingleQuestion, PyramidQuestion
from typing import Union


class QuestionFrame(tk.LabelFrame):
    content: tk.StringVar
    time: tk.IntVar  # time to answer in secounds
    point: tk.StringVar
    question_type_selection: ttk.Combobox
    question_list_pointer: list


    pen_color = 'red'##6162FF'

    def __init__(self, window, gui_controller, creator_controller, question_list_pointer, **kw):
        super().__init__(window, font=gui_controller.super_q_font, **kw)

        self.question_list_pointer = question_list_pointer
        self.creator_controller = creator_controller

        self.config(text='Pytanie ' + str(1 + len(self.question_list_pointer)))

        # self.scrollbar_arg = method

        self.content = tk.StringVar()
        question_content_label = tk.Label(self, text='Treść pytania', height=2)
        question_content_label.grid(row=0, column=0, sticky='W')
        self.entry_content = tk.Entry(self, textvariable=self.content, width=48,
                                      font=gui_controller.big_font, fg=self.pen_color, bd=0)
        self.entry_content.grid(row=0, column=1, sticky='W')

        question_type_label = tk.Label(self, text='Typ pytania', height=2)
        question_type_label.grid(row=1, column=0, sticky='W')
        self.question_type_selection = ttk.Combobox(self, state='readonly',
                                                    values=('Open', 'Multiple', 'Single', 'Pyramid'))
        self.question_type_selection.bind("<<ComboboxSelected>>", self.select_type)
        self.question_type_selection.current(0)
        self.question_type_selection.grid(row=1, column=1, sticky='W')

        time_to_ans_label = tk.Label(self, text='Czas na odpowiedź (w sekundach)', height=2)
        time_to_ans_label.grid(row=2, column=0, sticky='W')
        self.time = tk.IntVar()
        self.time.set('20')
        tk.Entry(self, textvariable=self.time, font=gui_controller.big_font, fg=self.pen_color, width=5, bd=0) \
            .grid(row=2, column=1, sticky='W')

        tk.Label(self, text='Punktacja za poprawną odpowiedź', height=2).grid(row=3, column=0, sticky='W')
        self.point = tk.StringVar()
        self.point.set('1')
        tk.Entry(self, textvariable=self.point, font=gui_controller.big_font, fg=self.pen_color, width=5, bd=0) \
            .grid(row=3, column=1, sticky='W')

        # remove question
        self.remove_button = tk.Button(self, text='Usuń pytanie', command=self.delete_question,
                                       width=10, bg='#6162FF', fg='white', bd=0)
        self.remove_button.grid(row=20, column=1, sticky='E')

        """ Instruction for each question type """
        self.labels_for_question_type = {'open': 'Podaj tagi, rozdzielając je przecinkiem \',\'',
                                         'multiple': 'Uzupełnij pola odpowiedzi i zaznacz docelowo prawidłowe.',
                                         'pyramid': 'Uzupełnij pola odpowiedzi i posortuj je odpowiednio',
                                         'single': 'Uzupełnij pola odpowiedzi i zaznacz docelowo prawidłową odpowiedź'}
        self.type_instruction = tk.Label(self, width=55, anchor='w')
        self.type_instruction.grid(row=4, columnspan=2, sticky='W')

        """ Open question """
        self.tags = tk.StringVar()
        self.tags_entry = tk.Entry(self, textvariable=self.tags, font=gui_controller.big_font, fg=self.pen_color,
                                   width=40, bd=0)

        """ Multiple question """
        self.check_buttons_toggles = []
        self.multiple_str_var = []
        self.check_buttons = []
        self.check_buttons_entries = []
        for i in range(4):
            toggle_var = tk.IntVar()
            string_var = tk.StringVar()
            string_var.set('Odpowiedź ' + str(i))
            check_button = tk.Checkbutton(self, variable=toggle_var)
            entry = tk.Entry(self, textvariable=string_var, width=48, font=gui_controller.big_font, fg='#6162FF', bd=0)
            self.check_buttons_toggles.append(toggle_var)
            self.multiple_str_var.append(string_var)
            self.check_buttons.append(check_button)
            self.check_buttons_entries.append(entry)

        """ Single """
        #  od 9-12
        self.radio_buttons = []
        self.radio_button_toggle = tk.IntVar()
        self.single_str_var = []
        self.single_entries = []
        for i in range(4):
            string_var = tk.StringVar()
            string_var.set('Odpowiedź ' + str(i))
            button = tk.Radiobutton(self, variable=self.radio_button_toggle, command=self.check_control, value=i)
            # command to delete
            entry = tk.Entry(self, textvariable=string_var, width=48, font=gui_controller.big_font, fg='#6162FF', bd=0)
            self.single_entries.append(entry)
            self.single_str_var.append(string_var)
            self.radio_buttons.append(button)

        """ Pyramid question """
        self.pyramid_entries = []
        self.pyramid_combos = []
        self.pyramid_questions = []
        self.pyramid_str_var = []
        for i in range(4):
            string_var = tk.StringVar()
            string_var.set('Odpowiedź ' + str(i))
            self.pyramid_str_var.append(string_var)
            entry = tk.Entry(self, textvariable=string_var, width=48, font=gui_controller.big_font, fg='#6162FF', bd=0)
            self.pyramid_entries.append(entry)
            self.pyramid_questions.append(string_var)

            pyramid_combo = ttk.Combobox(self, state='readonly', values=PyramidQuestion.get_pyramid_values(4), width=3,
                                         font=gui_controller.big_font)
            pyramid_combo.current(None)
            self.pyramid_combos.append(pyramid_combo)

        self.select_type('Open')

    def get_title(self) -> str:
        return self.content.get()

    def check_control(self) -> None:
        print('toggle', str(self.radio_button_toggle.get()))

    def select_type(self, eventObject):
        self.creator_controller.customize_scrollbar()
        if self.question_type_selection.get() == 'Multiple':
            self.clean_pyramid()
            self.clean_single()
            self.clean_open()
            self.change_to_multiple()
        elif self.question_type_selection.get() == 'Open':
            self.clean_pyramid()
            self.clean_single()
            self.clean_multiple()
            self.change_to_open()
        elif self.question_type_selection.get() == 'Single':
            self.clean_multiple()
            self.clean_open()
            self.clean_pyramid()
            self.change_to_single()
        elif self.question_type_selection.get() == 'Pyramid':
            self.clean_multiple()
            self.clean_open()
            self.clean_single()
            self.change_to_pyramid()
        else:
            print('ExamCreator.py ERROR IN COMBO BOX SELECTION :', eventObject)

    def change_to_multiple(self) -> None:
        self.type_instruction.configure(text=self.labels_for_question_type['multiple'])
        tmp = 0
        for ch_b in self.check_buttons:
            ch_b.grid(row=5 + tmp, column=0, sticky='S')
            tmp += 1
        tmp = 0
        for entry in self.check_buttons_entries:
            entry.grid(row=5 + tmp, column=1, sticky='S')
            tmp += 1

    def change_to_open(self) -> None:
        self.type_instruction.configure(text=self.labels_for_question_type['open'])
        self.tags_entry.grid(row=5, columnspan=2, sticky='S')

    def change_to_pyramid(self) -> None:
        self.type_instruction.configure(text=self.labels_for_question_type['pyramid'])
        tmp = 0
        for combo in self.pyramid_combos:
            combo.grid(row=13 + tmp, column=0, sticky='S')
            tmp += 1
        tmp = 0
        for entry in self.pyramid_entries:
            entry.grid(row=13 + tmp, column=1, sticky='S')
            tmp += 1

    def change_to_single(self) -> None:
        self.type_instruction.configure(text=self.labels_for_question_type['single'])
        tmp = 0
        for rb in self.radio_buttons:
            rb.grid(row=9 + tmp, column=0, sticky='S')
            tmp += 1
        tmp = 0
        for entry in self.single_entries:
            entry.grid(row=9 + tmp, column=1, sticky='S')
            tmp += 1

    def clean_pyramid(self):
        for combo in self.pyramid_combos:
            combo.grid_forget()
        for entry in self.pyramid_entries:
            entry.grid_forget()

    def clean_single(self):
        for rb in self.radio_buttons:
            rb.grid_forget()
        for entry in self.single_entries:
            entry.grid_forget()

    def clean_open(self):
        self.tags_entry.grid_forget()

    def clean_multiple(self):
        for ch_b in self.check_buttons:
            ch_b.grid_forget()
        for entry in self.check_buttons_entries:
            entry.grid_forget()

    def return_question(self) -> Union[Question, None]:
        if 'Open' == self.question_type_selection.get():  #
            return OpenQuestion(content=self.content.get(),
                                time=self.time.get(),
                                point=self.point.get(),
                                tags=self.tags.get())
        elif 'Multiple' == self.question_type_selection.get():
            return MultipleQuestion(content=self.content.get(),
                                    time=self.time.get(),
                                    point=self.point.get(),
                                    available_answers=self.get_many_answers(self.multiple_str_var),
                                    answers=self.get_many_answers(self.check_buttons_toggles))
        elif 'Pyramid' == self.question_type_selection.get():
            return PyramidQuestion(content=self.content.get(),
                                   time=self.time.get(),
                                   points=self.point.get(),
                                   available_answers=self.get_many_answers(self.pyramid_str_var),
                                   answers=self.get_many_answers(self.pyramid_combos))
        elif 'Single' == self.question_type_selection.get():
            print('return single')
            return SingleQuestion(content=self.content.get(),
                                  time=self.time.get(),
                                  point=self.point.get(),
                                  available_answers=self.get_many_answers(self.single_str_var),
                                  answer=self.radio_button_toggle.get())
        else:
            return None  # invalid type

    @staticmethod
    def get_many_answers(string_var_set) -> list:
        answers = []
        for i in string_var_set:
            answers.append(i.get())
        return answers

    def delete_question(self):
        self.question_list_pointer.remove(self)
        for q in self.question_list_pointer:
            q.config(text='Pytanie ' + str(1 + self.question_list_pointer.index(q)))
        self.destroy()

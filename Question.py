from dataclasses import dataclass
from typing import Union
from abc import ABC, abstractmethod


class Question(ABC):
    content: str
    time: int
    points: float
    pic: Union[None]

    @abstractmethod
    def return_as_dictionary(self):
        pass


@dataclass
class OpenQuestion(Question):
    tags: Union[list, None]
    answer: Union[None, str]

    def __init__(self, content, time, point, tags):
        self.content = content
        self.time = time
        self.points = point
        self.answer = tags.split(',')

    def return_as_dictionary(self):
        obj = {'type': 'OpenQuestion',
               'content': self.content,
               'time': self.time,
               'points': self.points,
               'answer': self.answer}
        return obj


@dataclass
class SingleQuestion(Question):
    available_answers: Union[list, None]
    answer: Union[int, None]
    number_of_answer_variants: int  # not more than 9 -> default 4

    def __init__(self, content, time, point, available_answers, answer):
        self.content = content
        self.time = time
        self.points = point
        self.available_answers = available_answers
        self.answer = answer

    def return_as_dictionary(self):
        obj = {'type': 'SingleQuestion',
               'content': self.content,
               'time': self.time,
               'points': self.points,
               'available_answers': self.available_answers,
               'answer': self.answer}
        return obj


@dataclass
class MultipleQuestion(Question):
    available_answers: list
    answer: Union[list, None]
    number_of_answer_variants: int  # not more than 9 -> default 4

    def __init__(self, content, time, point, available_answers, answers):
        self.content = content
        self.time = time
        self.points = point

        self.available_answers = available_answers
        self.answer = answers
        print(self.content)
        print(self.available_answers)
        print(self.answer)

    def return_as_dictionary(self):
        obj = {'type': 'MultipleQuestion',
               'content': self.content,
               'time': self.time,
               'points': self.points,
               'available_answers': self.available_answers,
               'answer': self.answer}
        return obj


@dataclass
class PyramidQuestion(Question):
    available_answers: list
    answer: Union[list, None]
    number_of_answer_variants: int  # not more than 9 -> default 4

    def __init__(self, content, time, points, available_answers, answers):
        self.content = content
        self.time = time
        self.points = points
        self.available_answers = available_answers
        self.answer = answers

    def return_as_dictionary(self):
        obj = {'type': 'PyramidQuestion',
               'content': self.content,
               'time': self.time,
               'points': self.points,
               'available_answers': self.available_answers,
               'answer': self.answer}
        return obj

    @staticmethod
    def get_pyramid_values(variable: int):
        var_list = []
        for i in range(variable):  # number_of_answer_variants
            var_list.append(i+1)
        return var_list


@dataclass
class MatchQuestion(Question):
    available_answers: list
    answer: Union[list, None]
    number_of_answer_variants: int  # not more than 9 -> default 4

    def __init__(self, content, time, point, available_answers, answers):
        self.content = content
        self.time = time
        self.point = point

    def return_as_dictionary(self):
        pass

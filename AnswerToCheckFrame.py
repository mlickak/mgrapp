import tkinter as tk
from tkinter import messagebox


class AnswerToCheckFrame(tk.LabelFrame):
    creator_controller: tk.Tk

    _points: tk.StringVar
    number: int
    is_open: bool

    def __init__(self, scrollbar_holder, controller_holder, number, q_content, a_content, is_correct, is_open, is_pyramid, is_multiple, **kw):
        super().__init__(scrollbar_holder, font=controller_holder.super_q_font, **kw)

        self.creator_controller = controller_holder
        self.is_open = is_open

        self.config(text='Pytanie ' + str(number))

        _question_content = tk.Label(self, text=q_content, height=1, width=65)
        _question_content.grid(row=0, column=0, columnspan=4, sticky='w')

        row_pointer = 1

        if is_open:
            # open
            l = tk.Label(self, text=a_content, wraplength=530, justify='left')
            l.grid(row=row_pointer, column=0, columnspan=2, sticky='w')
            row_pointer += 1

            # should be pointed
            points_label = tk.Label(self, text='Punkty za otwartą odpowiedź: ', height=2)
            points_label.grid(row=row_pointer, column=0, columnspan=2, sticky='w')
            self._points = tk.StringVar()
            self._points_entry = tk.Entry(self, textvariable=self._points, width=6, justify='center')
            self._points_entry.grid(row=row_pointer, column=1, sticky='w')
            max_label = tk.Label(self, text='pkt (na max 2.0 punktów)')
            max_label.grid(row=row_pointer, column=1)
            row_pointer += 1

        else:
            if is_multiple:
                if is_pyramid:
                    # pyramid
                    for i, a in enumerate(a_content):
                        l = tk.Label(self, text=(str(i+1) + ' ' + a))
                        if is_correct[i]:
                            l.config(fg='green')
                        else:
                            l.config(fg='red')
                        l.grid(row=row_pointer, column=0, columnspan=2, sticky='w')
                        row_pointer += 1
                else:
                    # multiple
                    for a in a_content:
                        l = tk.Label(self, text=a)
                        if is_correct:
                            l.config(fg='green')
                        else:
                            l.config(fg='red')
                        l.grid(row=row_pointer, column=0, columnspan=2, sticky='w')
                        row_pointer += 1

            else:
                # single
                l = tk.Label(self, text=a_content)
                if is_correct:
                    l.config(fg='green')
                else:
                    l.config(fg='red')
                l.grid(row=row_pointer, column=0, columnspan=2, sticky='w')
                row_pointer += 1
            # should be accepted

        # if type(question_content) == list and type(is_correct) == list:
        #         pointer += 1
        # elif type(question_content) == str:
        #     l = tk.Label(self, text='text')
        #     if is_correct:
        #         l.config(fg='green')
        #     else:
        #         l.config(fg='red')
        #     l.grid(row=pointer, column=0, columnspan=2, sticky='w')
        #     pointer += 1
        # else:
        #     print('bad ans type type')
        #

        # self.open_correct =
        # self._check_b = tk.Checkbutton(self)
        # question_content_label.grid(row=0, column=0, sticky='W', columnspan)
        #
        self._comment = tk.StringVar()
        comment_comment = tk.Label(self, text='Komentarz do zadania:')
        comment_comment.grid(row=row_pointer, column=0, columnspan=2, sticky='w')
        row_pointer += 1
        self._comment_entry = tk.Entry(self, textvariable=self._comment, width=70)
        self._comment_entry.grid(row=row_pointer, columnspan=4, sticky='w')

        # komunikat

    def checkpoint(self):
        if self.is_open:
            obj = self._points.get()
            if 0.0 > round(float(obj), 1) > 2.0:
                messagebox.showwarning(title='Nie można ocenić pytania numer 1',
                                       message='Punkty za pytanie nie mogą być ujemne ani większe od maksymalnej ilości punktów przypisanej do tego zadania')

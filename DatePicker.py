import tkinter as tk
import tkcalendar


class DatePicker(tk.Tk):

    def __init__(self, exam_date_hadler, button_handler):
        super().__init__()

        self.edh = exam_date_hadler
        self.b = button_handler

        date_frame = tk.Frame(self)
        date_frame.pack()

        self.new_calendar = tkcalendar.Calendar(date_frame, selectmode='day', year=2021, month=7, day=20)
        self.new_calendar.pack()

        ok_date_time = tk.Button(date_frame,
                                 text='Ok',
                                 command=self.return_data,
                                 width=12, bg='#6162FF', fg='white', bd=0)
        ok_date_time.pack()

        self.mainloop()

    def return_data(self):
        self.edh = self.new_calendar.get_date()
        self.b.config(text=self.edh)
        self.destroy()

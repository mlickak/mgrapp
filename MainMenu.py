import tkinter as tk
from Language import Language as txt


class MainMenu(tk.Frame):
    controller: tk.Tk
    main_color = '#6162FF'

    def __init__(self, parent, controller, bg):
        super().__init__(parent, bg=bg, width=controller.get_frame_width(), height=controller.get_frame_height())
        self.controller = controller

        button_width = 20

        """Main title"""
        self.title_label = tk.Label(self, text='Menu główne', font=self.controller.title_font, bg=bg)
        self.title_label.place(relx=0.5, rely=0.1, anchor=tk.CENTER)

        """Create exam button"""
        self.create_exam_button = tk.Button(self, text='Stwórz egzamin',
                                            command=lambda: self.controller.show_frame('ExamCreator'),
                                            font=self.controller.button_font, width=button_width, bg=self.main_color,
                                            fg=bg,
                                            bd=0)

        """ Approach to the exam """
        self.approach_button = tk.Button(self, text='Podejdź do egzaminu',
                                         command=lambda: self.controller.show_frame('ApproachToExam'),
                                         font=self.controller.button_font, width=button_width, bg=self.main_color,
                                         fg=bg, bd=0)

        """Sign out"""
        self.sign_out_button = tk.Button(self, text='Wyloguj się', command=lambda: self.controller.show_frame('SignIn'),
                                         font=self.controller.button_font, width=button_width, bg=self.main_color,
                                         fg=bg,
                                         bd=0)
        # self.sign_out_button.place(x=center_x, y=100)

        """Check the grade"""
        self.check_grade_button = tk.Button(self, text='Sprawdź ocenę', command=lambda: self.controller.show_frame('GradeCheck'),
                                         font=self.controller.button_font, width=button_width, bg=self.main_color,
                                         fg=bg,
                                         bd=0)

        """Evaluate exam"""
        self.evaluate_exam = tk.Button(self, text='Oceń egzamin', command=lambda: self.controller.show_frame('ExamCheck'),
                                         font=self.controller.button_font, width=button_width, bg=self.main_color,
                                         fg=bg,
                                         bd=0)

        """Quit app"""
        self.quit_button = tk.Button(self, text='Wyjdź z aplikacji', command=lambda: controller.destroy(),
                                     font=self.controller.button_font, width=button_width, bg=self.main_color,
                                     fg=bg,
                                     bd=0)
        # self.quit_button.place(x=center_x, y=130)

    def replace_widgets(self, user_type) -> None:
        queue = []
        top_y = 0.2
        distance_y = 0.06

        self.create_exam_button.place_forget()
        self.approach_button.place_forget()
        self.sign_out_button.place_forget()
        self.quit_button.place_forget()

        # print('user type to mm', user_type)

        if user_type == 'UserType.Professor' or user_type is None or user_type == '':
            queue.append(self.create_exam_button)
            queue.append(self.evaluate_exam)
        if user_type == 'UserType.Student' or user_type is None or user_type == '':
            queue.append(self.approach_button)
            queue.append(self.check_grade_button)
        queue.append(self.sign_out_button)
        queue.append(self.quit_button)

        for button in queue:
            button.place(relx=0.5, rely=top_y, anchor=tk.CENTER)
            top_y += distance_y

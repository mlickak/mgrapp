"""
>>> print('yyy')
yyy

>>> q=OpenQuestion(content='pytanie', time=15, point=2, tags=None)
>>> print(q.tags)
None

>>> typeof(SingleQuestion.answer)
"""

def singleton(class_):
    instances = {}

    def get_instance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return get_instance


@singleton
class DatabaseManager:
    pass

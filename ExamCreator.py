import json
import tkinter as tk
from tkinter import ttk, messagebox
import Question
from Language import Language as lng
from Exam import Exam
from QuestionFrame import QuestionFrame
from requests.exceptions import HTTPError
import json
import requests
from typing import Final
from DatePicker import DatePicker


class ExamCreator(tk.Frame):
    controller: tk.Tk
    questions_in_frames: list
    dict_questions: list
    canvas: tk.Canvas
    y_scrollbar: ttk.Scrollbar
    scrollbar_frame: tk.Frame

    def __init__(self, parent, controller, bg):
        super().__init__(parent, bg=bg)
        self.controller = controller

        self.questions_in_frames = []
        self.dict_questions = []

        side_panel = 0.05
        main_panel = 0.25

        """top"""
        self.title_label = tk.Label(self, text='Tworzenie nowego egzaminu\n', font=self.controller.title_font,
                                    bg='white')
        self.title_label.place(relx=0.5, rely=0.1, anchor=tk.CENTER)

        """exam title """
        self.exam_title = tk.StringVar()

        # title label
        self.title_label = self.label = tk.Label(self, text='Tytuł egzaminu', bg=bg)
        self.title_label.place(relx=main_panel, rely=0.15, anchor='w')

        # title entry
        self.title_entry = tk.Entry(self, textvariable=self.exam_title, font=controller.big_font, fg='red',
                                    width=50)
        self.title_entry.place(relx=main_panel, rely=0.20, anchor='w')

        """ import exam """
        self.import_button = tk.Button(self, text='Importuj egzamin', command=self.import_past_exam,
                                       width=14, bg='#6162FF', fg=bg, bd=0)
        self.import_button.place(relx=main_panel+0.56, rely=0.20, anchor='w')

        """ add another question """
        self.add_question_label = tk.Label(self, text='Kliknij przycisk \'Dodaj\' aby dodać nowe pytanie', bg=bg)
        self.add_question_label.place(relx=main_panel, rely=0.26, anchor='w')
        self.add_question_button = tk.Button(self, text='Dodaj', command=self.add_new_question,
                                             width=10, bg='#6162FF', fg='white', bd=0)
        self.add_question_button.place(relx=main_panel, rely=0.31, anchor='w')  # pack()

        """Scroll bar"""
        wrapper1 = tk.LabelFrame(self)
        wrapper2 = tk.LabelFrame(self)

        self.canvas = tk.Canvas(wrapper1, width=530, height=320)
        self.canvas.pack(side=tk.LEFT)

        self.y_scrollbar = ttk.Scrollbar(wrapper1, orient='vertical', command=self.canvas.yview)
        self.y_scrollbar.pack(side=tk.RIGHT, fill='y')

        self.canvas.configure(yscrollcommand=self.y_scrollbar.set)
        self.canvas.bind('<Configure>', lambda e: self.canvas.configure(scrollregion=self.canvas.bbox('all')))

        self.scrollbar_frame = tk.Frame(self.canvas)
        self.canvas.create_window((0, 0), window=self.scrollbar_frame, anchor='nw')

        wrapper1.place(relx=main_panel, rely=0.67, anchor='w')  # pack(fill='both', expand='yes', padx=10, pady=10)
        wrapper2.place(relx=main_panel, rely=0.67, anchor='w')  # pack(fill='both', expand='yes', padx=10, pady=10)
        # wrapper1.pack(fill='both', expand='yes', padx=10, pady=10)
        # wrapper2.pack(fill='both', expand='yes', padx=10, pady=10)

        """Create button"""
        self.create_button = tk.Button(self, text='Stwórz egzamin', command=self.create_exam_controller,
                                       font=self.controller.button_font, width=12, bg='#6162FF', fg='white', bd=0)
        self.create_button.place(relx=side_panel, rely=0.15, anchor='w')

        """Quit button"""
        self.quit_button = tk.Button(self, text='Wyjdź do menu', command=self.quit_button_controller,
                                     font=self.controller.button_font, width=12, bg='#6162FF', fg='white', bd=0)
        self.quit_button.place(relx=side_panel, rely=0.21, anchor='w')

        self.adding_student_table = tk.Label(self, text='Brak dodanych studentów. Kliknij "Dodaj studenta" by '
                                                        'przypisać studenta do egzaminu', width=12, bg=bg,
                                             wraplength=90, justify='left')
        self.adding_student_table.place(relx=side_panel, rely=0.62, anchor='w')
        self.add_student = tk.Button(self, text='Dodaj studenta', command=self.add_student_controller,
                                     font=self.controller.button_font, width=12, bg='#6162FF', fg='white', bd=0)
        self.add_student.place(relx=side_panel, rely=0.5, anchor='w')

        self.student_list_for_this_exam = []

        """ Date """
        self.hour_button = self.add_student = tk.Button(self, text='hh',
                                                              command=lambda: self.time_changer('h'),
                                                              width=4, bg='#6162FF', fg='white', bd=0)
        self.hour_button.place(relx=main_panel + 0.19, rely=0.36, anchor='w')
        self.label_hour_minute = tk.Label(self, text=':', bg=bg)
        self.label_hour_minute.place(relx=main_panel + 0.24, rely=0.36, anchor='w')
        self.minute_button = self.add_student = tk.Button(self, text='mm',
                                                        command=lambda: self.time_changer('m'),
                                                        width=4, bg='#6162FF', fg='white', bd=0)
        self.minute_button.place(relx=main_panel + 0.26, rely=0.36, anchor='w')
        self.date_button = self.add_student = tk.Button(self, text='Wybierz datę',
                                                        command=self.date_picker,
                                                        width=12, bg='#6162FF', fg='white', bd=0)
        self.date_button.place(relx=main_panel + 0.35, rely=0.36, anchor='w')

        self.exam_date = None
        self.hour_start = None
        self.minute_start = None

    def import_past_exam(self):
        index = self.exam_title.get()

        url = self.controller.get_url() + '/manage/get-past-exam'
        obj = {'id': index,
               'token': self.controller.get_token()}
        json_obj = json.dumps(obj, indent=4)

        try:
            r = requests.get(url, json=json_obj)
            text_var = r.text
            if text_var == 'emergency_logout':
                self.controller.emergency_logout()
            else:
                #  import procedure
                pass
        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')  # Python 3.6
        except Exception as err:
            print(f'Other error occurred: {err}\nDatabase is propably off')  # Python 3.6
        except:
            print('Request send failed. Try again.')

    def time_changer(self, want_type):
        if want_type == 'h':
            if self.hour_start is None:
                self.hour_start = 12
            else:
                if self.hour_start == 24:
                    self.hour_start = 1
                else:
                    self.hour_start += 1
            self.hour_button.config(text=str(self.hour_start))

        elif want_type == 'm':
            if self.minute_start is None:
                self.minute_start = 0
            else:
                if self.minute_start == 55:
                    self.minute_start = 0
                else:
                    self.minute_start += 5

            if self.minute_start < 10:
                self.minute_button.config(text='0' + str(self.minute_start))
            else:
                self.minute_button.config(text=str(self.minute_start))
        else:
            pass

    def date_picker(self):
        DatePicker(self.exam_date, self.date_button)

    def ok_add_student(self):
        print(len(self.ok_entrance.get()))
        if len(self.ok_entrance.get()) == 6:
            self.student_list_for_this_exam.append(self.ok_entrance.get())
            string = ''
            for st in self.student_list_for_this_exam:
                string = string + st + '\n'
            self.adding_student_table.config(text=string, fg='red')
            self.index_window.destroy()

    def add_student_controller(self):
        self.index_window = tk.Tk()
        # index_window.config(heigh=20, width=100)
        frame = tk.Frame(self.index_window)
        frame.pack()
        label = tk.Label(frame, text='Wpisz indeks studenta:', width=30)
        label.pack()
        self.ok_entrance = tk.Entry(frame, width=6)
        self.ok_entrance.pack()
        ok_student = tk.Button(frame, text='Ok', command=self.ok_add_student,
                               width=12, bg='#6162FF', fg='white', bd=0)
        ok_student.pack()
        self.index_window.mainloop()

    """Adding new template question to scroll"""

    def add_new_question(self) -> None:
        new_question = QuestionFrame(self.scrollbar_frame, self.controller, self, self.questions_in_frames)
        new_question.pack(fill='x', anchor=tk.CENTER)
        self.questions_in_frames.append(new_question)

        self.customize_scrollbar()

    def customize_scrollbar(self) -> None:
        self.canvas.configure(yscrollcommand=self.y_scrollbar.set, scrollregion=self.canvas.bbox("all"))
        self.canvas.create_window((0, 0), window=self.scrollbar_frame, anchor='nw')

    def verify_exam(self):
        problem_list = []

        if len(self.exam_title.get()) == 0:
            problem_list.append('Egzaminu musi posiadać tytuł')
        if len(self.questions_in_frames) < 3:  # przynajmniej 3 pytania
            problem_list.append('Egzamin musi zawierać co najmniej 3 pytania')
        if len(self.student_list_for_this_exam) == 0:
            problem_list.append('Egzamin musi mieć przypisanych studentów')
        if self.hour_start is None or self.minute_start is None or self.exam_date is None:
            problem_list.append('Błędna data rozpoczęcia egzaminu')

        if len(problem_list) != 0:
            return False, problem_list
        else:
            return True, None

    """Creator"""
    def create_exam_controller(self) -> None:

        problem_check, problem_list = self.verify_exam()

        # try to create exam
        if problem_check is True:
            self.dict_questions = []

            for q in self.questions_in_frames:  # oddaj gotowe pytania z ramki
                # print('type1 ', type(q))
                question = q.return_question()
                # print('type2 ', type(question))
                dict_q = question.return_as_dictionary()
                # print(dict_q)
                self.dict_questions.append(dict_q)

            exam = Exam(self.exam_title.get(), self.dict_questions)
            json_exam = exam.get_json()
            self.sent_to_database(json_exam)
        else:
            string_problems = ''
            for i, x in enumerate(problem_list):
                string_problems += str(i) + ' ' + str(x) + '\n'
            messagebox.showerror(title='Nie można stworzyć egzaminu', message=string_problems)

    def quit_button_controller(self) -> None:
        self.controller.show_frame('MainMenu')

        # cleaning
        self.exam_title.set('')
        if len(self.dict_questions) != 0:
            self.dict_questions = []
            print('WARNING: question_list was not empty')
        if len(self.questions_in_frames) != 0:
            for p in self.questions_in_frames:
                p.destroy()
            print('WARNING: question_frames was not empty')
            self.questions_in_frames = []

    def sent_to_database(self, json_obj: str) -> None:
        """json.loads take a string as input and returns a dictionary as output.
        json.dumps take a dictionary as input and returns a string as output."""
        url = self.controller.get_url() + '/manage/new-exam/' + 'tokenoeno'  # self.controller.get_token()
        try:
            response = requests.post(url,
                                     json=json_obj)  # self.controller.get_token()})   , data={'token': 'tokenoeno'}
            # print(response.status_code)
            if response.status_code == 404:
                print('bad gateway')
            else:
                # print(response.text)
                self.controller.set_token(response.text)
                self.quit_button_controller()

        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')  # Python 3.6
        except Exception as err:
            print(f'Other error occurred: {err}\nDatabase is propably off')  # Python 3.6
        except:
            print('Request send failed. Try again.')

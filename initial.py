from GuiController import GuiController
import wmi  # Windows Management Instrumentation

wmi_connection = wmi.WMI()
# connecting to remote machines
# conn = wmi.WMI("13.76.128.231", user=r"prateek", password="P@ssw0rd@123")
windows_update_services_list = [
    "wuauserv",
    "TrustedInstaller"
]


if __name__ == '__main__':
    # for service_name in windows_update_services_list:
    #     service = wmi_connection.Win32_Service(Name=service_name)[0]
    #     service.ChangeStartMode(StartMode="Disabled")

    gui = GuiController()
    # gui.bind('<KeyRelease>', escape_through_window_counter)
    gui.mainloop()

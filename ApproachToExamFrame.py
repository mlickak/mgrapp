import json
import tkinter as tk
from tkinter import ttk, messagebox
import Question
from Language import Language as lng
from Exam import Exam
from QuestionFrame import QuestionFrame
from requests.exceptions import HTTPError
import json
import requests
from typing import Final
from typing import Union
from AnswerFrame import AnswerFrame
import random
import threading


class ApproachToExam(tk.Frame):
    controller: tk.Tk
    question_list: list
    answerFrame: Union[AnswerFrame, None]
    timer: threading

    exam_index: tk.StringVar
    index_control: int
    answers: list

    win_counter = 0
    f_active_exam = False

    button_length = 14

    # bg = 'white'

    def __init__(self, parent, controller, bg):
        super().__init__(parent, bg='white')
        self.controller = controller
        self.index_control = 0
        self.question_list = []

        """ frame title """
        title_label = tk.Label(self, text='Podejście do egzaminu\n', font=self.controller.title_font, bg='white')
        title_label.place(relx=0.5, rely=0.1, anchor=tk.CENTER)

        """ end button """
        self.end_button = tk.Button(self, text='Zakończ egzamin', command=lambda: self.end_button_controller(False),
                                    font=self.controller.button_font, width=self.button_length, bg='#6162FF',
                                    fg='white', bd=0)

        """ forward """
        self.forward_button = tk.Button(self, text='Następne\npytanie', command=self.go_forward,
                                        font=self.controller.button_font, width=8, height=3,
                                        bg='#6162FF', fg='white', bd=0)

        """ getting index to import exam """
        self.exam_index = tk.StringVar()
        self.exam_index.set('652407')

        self.index_label = tk.Label(self, text='Wprowadz indeks egzaminu:', font=self.controller.bigger_font, bg=bg)
        self.index_entry = tk.Entry(self, textvariable=self.exam_index, font=self.controller.bigger_font, fg='#6162FF',
                                    width=6, bg=bg)
        self.index_button = tk.Button(self, text='Wprowadź', command=self.import_exam,
                                      font=self.controller.button_font, width=self.button_length, bg='#6162FF',
                                      fg='white', bd=0)

        """ prepare """
        self.title = tk.Label(self, font=self.controller.title_font_2, bg='white')

        self.answerFrame = AnswerFrame(self, self.controller)

        self.set_input_data_getters()

    def set_input_data_getters(self):
        self.title.place_forget()
        self.forward_button.place_forget()
        self.end_button.place_forget()

        self.answerFrame.place_forget()
        self.end_button.place(relx=0.5, rely=0.61, anchor=tk.CENTER)
        self.index_label.place(relx=0.5, rely=0.35, anchor=tk.CENTER)
        self.index_entry.place(relx=0.5, rely=0.4, anchor=tk.CENTER)
        self.index_button.place(relx=0.5, rely=0.55, anchor=tk.CENTER)

    def set_widgets_to_exam_start(self):
        self.index_label.place_forget()
        self.index_entry.place_forget()
        self.index_button.place_forget()

        self.title.place(relx=0.5, rely=0.15, anchor=tk.CENTER)
        self.forward_button.place(relx=0.835, rely=0.5, anchor='w')
        self.end_button.place_forget()
        self.end_button.place(relx=0.5, rely=0.85, anchor=tk.CENTER)
        self.answerFrame.place(relx=0.5, rely=0.25, anchor='n')

        self.f_active_exam = True
        self.start_exam_timer_thread()
        self.go_forward()

    def go_forward(self):
        if self.forward_button['state'] == 'disabled':
            self.end_button_controller(True)
            # end exam click
        else:
            self.answerFrame.change_question(self.question_list[self.index_control], self.index_control,
                                             len(self.question_list))
            self.index_control = self.index_control + 1
            if self.index_control == len(self.question_list):
                # print('click no more')
                self.forward_button['state'] = 'disabled'
                self.forward_button.config(bg='light gray')

    def import_exam(self):
        """ if data are ok -> no need to send wrong data"""
        if not self.could_be_integer(self.exam_index.get()):
            messagebox.showerror(title='Błąd', message='Niepoprawny indeks')
        elif len(self.exam_index.get()) != 6:
            messagebox.showerror(title='Błąd', message='Indeks za długi lub za krótki')
        else:
            url = self.controller.get_url() + '/manage/get-template/' + str(self.exam_index.get())
            try:
                response = requests.get(url)
                if response.status_code == 200:
                    dict_obj = response.json()
                    if len(dict_obj) > 0:
                        self.title.config(text=dict_obj['title'])  # set title
                        self.question_list = json.loads(dict_obj['questions'])  # load question list
                        for i in range(len(self.question_list)):
                            self.question_list[i]['place'] = i
                        random.shuffle(self.question_list)  # zmiana kolejności pytań
                        self.answers = [None] * len(self.question_list)
                        print(self.answers)
                        self.answerFrame.set_answer_handle_list(self.answers)
                        self.set_widgets_to_exam_start()
                    else:
                        print('Incorrect data')
                else:
                    print('No response')
            except:
                pass

    def end_button_controller(self, definitely_end: bool):
        if len(self.question_list) == 0:
            pass
        elif not definitely_end:  # czy na pewno?
            if not messagebox.askokcancel('Zapytanie', 'Czy na pewno chcesz zakończyć?'):
                return
            else:
                self.answerFrame.safe_result()
                self.sending_to_database()
        else:
            self.answerFrame.safe_result()
            self.sending_to_database()
        self.f_active_exam = False
        self.controller.show_frame('MainMenu')

    def sending_to_database(self):
        print('unsorted')
        for q in self.answers:
            print(q)
        sorted_answers = sorted(self.answers, key=lambda k: k['place'])
        print('sorted')
        for q in sorted_answers:
            print(q['place'])

        dict_exam = {'id': self.exam_index.get(),
                     'answers': sorted_answers}
        json_exam = json.dumps(dict_exam, indent=4)
        url = self.controller.get_url() + '/manage/receive-complete-exam/' + self.controller.get_token()
        try:
            response = requests.post(url, json=json_exam)
            print(response.text)

            # spróbuj wysłać odp
            # jeśli nie przeszła
            # zrób jsona w pliku zakodowanego
            # zapisz wynik egzaminu lokalnie
            # jeśli udało się wysłać do bazy danych wyczyść jsona
            # jeśli nie - komunikat

        except HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')  # Python 3.6
        except Exception as err:
            print(f'Other error occurred: {err}\nDatabase is propably off')  # Python 3.6
        except:
            print('Request send failed. Try again.')
        finally:
            self.cleaning_after()

    def cleaning_after(self):
        self.index_control = 0
        self.title.config(text='')
        self.question_list = []
        self.set_input_data_getters()
        self.controller.show_frame('MainMenu')

    def window_escape(self):
        self.win_counter += 1
        if not self.f_active_exam:  # if egzamin aktywny
            if self.win_counter == 1:
                print(self.win_counter)
                print('WARNING')
                # ostrzeżenie
                messagebox.showwarning(title='Ostrzeżenie', message='Kolejna próba '
                                                                    'wyjścia z aplikacji w trakcie egzaminu'
                                                                    ' automatycznie zakończy egzamin')
                if self.controller.state() == "iconic":
                    self.controller.lift()

            elif self.win_counter > 1:
                print(self.win_counter)
                print('close')
                # self.end_button_controller(True)
                self.win_counter = 0
            else:
                pass
        else:
            if self.win_counter != 0:
                self.win_counter = 0

    @staticmethod
    def could_be_integer(my_str: str) -> bool:
        try:
            int(my_str)
            return True
        except ValueError:
            return False

    def start_exam_timer_thread(self) -> None:
        self.timer = threading.Thread(target=self.exam_timer, args=())  # time for exam
        self.timer.start()

    def exam_timer(self):
        refreshing = 1
        try:
            # if examtime > 0:
            if self.controller.state() == "iconic":
                self.window_escape()
                self.controller.state
            #elif not f_active_exam
            # end thread break
            #elif examtime <= 0 and f_active_exam:
            # end exam
            # else:
            #     print('time problems in approaching to exam')
        except RuntimeError:
            pass

# @staticmethod
# def add_placeholder(entry, string_var, placeholder):
#     if string_var.get() == '':
#         string_var.set(placeholder)
#         entry.config({'fg': 'Silver'})
#
# @staticmethod
# def erase_placeholder(entry, string_var, placeholder):
#     if string_var.get() == placeholder:
#         string_var.set('')
#         entry.config({'fg': 'Black'})

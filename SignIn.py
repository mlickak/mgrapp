import tkinter as tk
from Language import Language as txt
import requests
import socket
from getmac import get_mac_address as gma
from requests.exceptions import HTTPError
import json
from MainMenu import MainMenu


class SignIn(tk.Frame):
    controller: tk.Tk
    var_login: tk.StringVar
    var_password: tk.StringVar

    main_color = '#6162FF'
    entry_bg = '#ececec'

    def __init__(self, parent, controller, bg):
        super().__init__(parent, bg=bg, width=controller.get_frame_width(), height=controller.get_frame_height())
        self.place(x=0, y=0)
        self.controller = controller
        # self.configure(width=300, height=100)

        self.var_login = tk.StringVar()
        self.var_password = tk.StringVar()

        """Title"""
        self.label = tk.Label(self, text='Panel logowania', font=self.controller.title_font, bg=bg, fg=self.main_color)
        self.label.place(relx=0.5, rely=0.1, anchor=tk.CENTER)

        """Username label"""
        self.label_username = tk.Label(self, text='Nazwa użytkownika : ', bg=bg,
                                       font=self.controller.font_to_write_with)
        self.label_username.place(relx=0.3, rely=0.2, anchor='w')

        """Username entry"""
        self.login_entry = tk.Entry(self, textvariable=self.var_login, width=35, bg=self.entry_bg)
        self.login_entry.place(relx=0.5, rely=0.25, anchor=tk.CENTER)

        """Password label"""
        self.label_password = tk.Label(self, text='Hasło : ', bg=bg,
                                       font=self.controller.font_to_write_with)
        self.label_password.place(relx=0.3, rely=0.3, anchor='w')

        """Password entry"""
        self.password_entry = tk.Entry(self, textvariable=self.var_password, width=35, bg=self.entry_bg)
        self.password_entry.config(show='*')  # hiding password
        self.password_entry.place(relx=0.5, rely=0.35, anchor=tk.CENTER)

        """Sign in button"""
        self.log_in_button = tk.Button(self, text='Zaloguj', command=self.sign_in_button_controller,
                                       font=self.controller.button_font, width=10, bg=self.main_color, fg=bg,
                                       bd=0)
        self.log_in_button.place(relx=0.6, rely=0.45, anchor=tk.CENTER)#(x=300, y=250)

        """Quit button"""
        self.quit_button = tk.Button(self, text='Wyjdź', command=lambda: controller.destroy(),
                                     font=self.controller.button_font, width=10, bg=self.main_color, fg=bg, bd=0)
        self.quit_button.place(relx=0.6, rely=0.51, anchor=tk.CENTER)

        """Help label"""
        # text = 'helping comment to seeeee\n' \
        #        '-> dis is to seee\n' \
        #        '-> diss to to seeeeeeeeeeeeeeeeeeeeee'
        # self.label_password = tk.Label(self, text=text, bg=self.bg, anchor='e',
        #                                font=self.controller.font_to_write_with)
        # self.label_password.place(relx=0.5, rely=0.65, anchor=tk.CENTER)

    def sign_in_button_controller(self) -> None:
        # print('index :', self.var_login.get(), '\npassword :', self.var_password.get())

        mac = gma()  # mine = 00:24:d7:f6:35:9c
        ip = socket.gethostbyname(socket.gethostname())  # mine = 192.168.0.73

        self.controller.reset_user_type(self.var_login.get())
        # print('sent type req')
        self.var_login.set('')
        self.controller.show_frame('MainMenu')  # change the panel

        # if self.var_login.get() == '' or self.var_password.get() == '':  # do not send empty input
        #     return ''
        # else:
        #     url = self.controller.get_url() + '/auth/sign-in'
        #     obj = {
        #         'id': self.var_login.get(),
        #         'password': self.var_password.get()}
        #     json_obj = json.dumps(obj, indent=4)
        #
        #     try:
        #         r = requests.get(url, json=json_obj)
        #         token = r.text
        #         if len(token) != 0:
        #             self.controller.set_token(token)
        #             print('Signed in')
        #             self.controller.show_frame('MainMenu')  # change the panel
        #
        #         else:
        #             print('Not signed in')
        #             pass

            # except HTTPError as http_err:
            #     print(f'HTTP error occurred: {http_err}')  # Python 3.6
            # except Exception as err:
            #     print(f'Other error occurred: {err}\nDatabase is propably off')  # Python 3.6
            # except:
            #     print('Request send failed. Try again.')
        self.var_password.set('')
